<div align="center">
  <a id="readme"></a>
  <img src="https://assets.dyff.io/logo-box.svg" alt="Dyff" width=128 height=128/>
  <h1>Dyff AI Auditing Platform</h1>
  <h3>Open science infrastructure for long-lived safety assessments.</h3>
  <hr/>
</div>

## About

Dyff is an open-source platform for running reproducible audits of AI/ML systems. Built on [Kubernetes](https://kubernetes.io/) and other open-source infrastructure technologies, the Dyff platform can orchestrate the computational resources needed to run large AI/ML models against suites of input datasets and performance measures. Dyff interacts with the system under test as a Web service running in one or more Docker containers, allowing Dyff to audit production-ready AI/ML systems that may have multiple components and complex dependencies. Dyff enables auditing using **private datasets** that are never revealed outside of the platform, which preserves their diagnostic power by preventing training on the test set.

You can find documentation and the latest project news at the [project website](https://dyff.io/).

## Status

Dyff is research code under active development. Dyff is currently in a **pre-alpha** state.

> Do not use this software unless you are an active collaborator on the associated research project.
> 
> This project is an output of an ongoing, active research project. It is published without warranty, is subject to change at any time, and has not been certified, tested, assessed, or otherwise assured of safety by any person or organization. Use at your own risk.

## Start using Dyff

The [dyff/dev-environment](https://gitlab.com/dyff/dev-environment) repository contains an infrastructure-as-code (IaC) deployment of the Dyff Platform on a single machine using [kind](https://kind.sigs.k8s.io/). This deployment is tested on Ubuntu 22.04.

Production-ready deployments targeting major cloud computing platforms are under development.
